Role Name
=========

Установка и настройка auditd

Example Playbook
----------------

    - hosts: servers
	  become: yes
	  
	  roles:
	    - auditd

License
-------

MIT

Author Information
------------------

Konstantin Khazov <khazovkv@gmail.com>
